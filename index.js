const pluginName = 'PluginUpdateVersionTipName';
class UpdateVersionTipPlugin {
    constructor(options = {}) {
        this.timestamp =options.version|| new Date().getTime();
        this.updateMark = options.updateMark || 'version'
        this.checkUpdateFnName = options.checkUpdateFnName || 'checkUpdate'
        this.filePath = options.filePath ||'/'
    }
    createFnJs() {
        let fnStr = `window.${this.checkUpdateFnName} = function () {
            return new Promise((resolve, reject) => {
                let scriptEl = document.createElement('script', { async: true })
                scriptEl.addEventListener("load", () => {
                    let currentvVersion = localStorage.getItem("${this.updateMark}") || null
                    if ( window.${this.updateMark} && currentvVersion) {
                        if ( window.${this.updateMark} !== parseInt(currentvVersion)) {
                            resolve({
                                newFlag:window.${this.updateMark},
                                olfFlag:currentvVersion,
                                update:(fn =()=>{})=>{
                                   return new Promise((res,rej)=>{
                                    try{
                                        window.localStorage.setItem("${this.updateMark}", window.${this.updateMark})
                                        res(fn&&fn(res,rej))
                                    }catch(e){
                                        rej(e)
                                    }
                                   })
                                }
                            })
                        }
                    } else {
                        if( window.${this.updateMark}){
                            window.localStorage.setItem("${this.updateMark}", window.${this.updateMark})
                        }
                    }
                    reject();
                    scriptEl.remove()
                });
                document.body.appendChild(scriptEl)
                scriptEl.src = '${this.filePath}version.js?time='+ new Date().getTime()
            })
        }`
        return fnStr
    }
    createUpdateFlag() {
        let content = `window.${this.updateMark}=${this.timestamp}
        `;
        return content
    }
    apply(compiler) {
        compiler.hooks.emit.tapAsync(pluginName, (compilation, callback) => {
            let customScriptContent = this.createFnJs()
            let createUpdateFlagScriptContent = this.createUpdateFlag()
            compilation.assets[`${this.filePath}${this.checkUpdateFnName}.js`] = {
                source: () => customScriptContent,
                size: () => Buffer.byteLength(customScriptContent)
            };
            compilation.assets[`${this.filePath}${this.updateMark}.js`] = {
                source: () => createUpdateFlagScriptContent,
                size: () => Buffer.byteLength(createUpdateFlagScriptContent)
            };
            callback();
        });
    }
}

module.exports = UpdateVersionTipPlugin;