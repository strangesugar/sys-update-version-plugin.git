# sys-update-version-plugin

#### 介绍

一个仅前端打包部署后检测更新的 webpack 插件，

#### 安装教程

##### 1. 下载插件 <br>

###### npm install sys-update-version-plugin

##### 2. 在 webpack 或者脚手架中使用

###### const UpdateVersionTip = require('sys-update-version-plugin')<br>

    module.exports = {
        //...
        plugins: [
            //默认可以不写options
            new UpdateVersionTip({
                // options...
                }),
            ],
        };

    options见下方配置

#### 注意事项

###### 每次打包之后一定要确认打包文件的 hash 值变了，这样系统加载的 js 文件才是最新的，不然服务器可能会走缓存返回之前的 js 文件。一般在打包时在文件名上添加时间戳，如下：<br>

//该配置不是通用配置，需根据自己项目情况百度或者查看文档

###### config.output.filename(`static/js/[hash].[name].${时间戳}.js`).end();

#### 使用说明
##### 1. option   使用时可以不配置，默认值如下：
    const options = {
        version: '不指定版本时，为当前打包时间戳，默认值打包时间戳',
        updateMark: '标记系统的版本号字段，默认值‘version’',
        checkUpdateFnName: '校验更新的函数名，默认值‘checkUpdate’',
        filePath: '插件的配置文件放置位置，默认值‘/’',
    }



 ##### 2. 在项目的 index.html 中引加载，注意：不是打包后的 index.html，而是打包前的 index.html，可能在你的根目录或者是 public 目录下
    手动添加 <script src="/checkUpdate.js"></script>（默认）
    或者 <script src="{你的filePath}{你的checkUpdateFnName}.js"></script>（使用“filePath”和“checkUpdateFnName” 的配置值 ）

##### 3. 检测是否有新版本
 在项目的 “适当位置” 添加如下代码 （一般选择在路由调整时执行）

        //防止报错
        if(!window.checkUpdate) return
        //主要代码
        window.checkUpdate().then(({update})=>{
            //如果需要更新则会进入这里
            //处理其他更新逻辑，如更新提示
            alert("有新版本了，请及时更新")
            //需手动调用更新方法，更新系统版本标签
            update&&update().then(()=>{
            //更新标签完成后可以做的事情
            window.location.reload()
            })
        })

4.  有问题可以联系我邮箱：1004709307@qq.com
